"""Main file for the house security bot."""
import logging
import cv2
import json
import time
import imutils
from datetime import datetime
from picamera import PiCamera
from picamera.array import PiRGBArray
from os import mkdir, path, remove
from telegram.ext import CommandHandler, Updater

PATH = path.dirname(path.abspath(__file__))


def password(f):
    """Check if this chat has entered the correct password."""
    def wrapper(self, update, context):
        if self.info.is_valid(update.effective_chat.id):
            f(self, update, context)
    return wrapper


class NonVolatileInformation:
    """Used for storing chat information in a json file."""

    def __init__(self, file):
        """Get the information currently stored in the file."""
        self.file = file
        with open(self.file, 'r') as fp:
            self._info = json.load(fp)
        self.token = self._info['token']

    def create_new(self, chat_id):
        """Create a new chat with default information."""
        self[chat_id] = {'enabled': True, 'silent': False}

    def __getitem__(self, chat_id):
        """Get the information about a given chat."""
        return self._info['chats'][str(chat_id)]

    def __setitem__(self, chat_id, state):
        """Set the information about a given chat (only works sometimes)."""
        self._info['chats'][str(chat_id)] = state
        with open(self.file, 'w') as fp:
            json.dump(self._info, fp)

    def _update(self):
        with open(self.file, 'w') as fp:
            json.dump(self._info, fp)

    def setitem(self, chat_id, arg_type, state):
        """Set the information about a given chat."""
        self._info['chats'][str(chat_id)][arg_type] = state
        self._update()

    def toggle(self, chat_id, arg_type):
        """Toggle the information about a given chat."""
        self._info['chats'][str(chat_id)][arg_type] = \
            not self._info['chats'][str(chat_id)][arg_type]
        self._update()

    def __iter__(self):
        """Iterate through the chat ids."""
        return iter(self._info['chats'])

    def is_valid(self, chat_id):
        """Check if a given chat id is validated."""
        return str(chat_id) in self._info['chats'].keys()


class TempImage:
    """Creates and destroys a temporary image file."""

    def __init__(self, frame):
        """Save the frame."""
        self.frame = frame

    def __enter__(self):
        """Create the image."""
        self.path = f'{PATH}/temp_image.jpg'
        cv2.imwrite(self.path, self.frame)
        logger.info('Created a temporary image.')
        self.fd = open(self.path, 'rb')
        return self.fd

    def __exit__(self, type, value, traceback):
        """Remove the image."""
        self.fd.close()
        remove(self.path)


class TelegramBot:
    """Control all the aspects of the telegram bot side of it."""

    def __init__(self, logger):
        """Set up the necessary functions and operations."""
        self.logger = logger
        self.info = NonVolatileInformation(f'{PATH}/telegram_info.json')
        self.pause_info = {chat_id: {'enabled': None, 'silent': None}
                           for chat_id in self.info}
        self.updater = Updater(self.info.token)
        self.dispatcher = self.updater.dispatcher
        self._add_command('start', self._start)
        self._add_command('disable', self._disable)
        self._add_command('enable', self._enable)
        self._add_command('status', self._status)
        self._add_command('picture', self._picture)
        self._add_command('silent', self._silent)
        self.updater.start_polling()
        self.logger.info('Started Polling Telegram Bot')

    def _add_command(self, name, func):
        self.dispatcher.add_handler(CommandHandler(name, func))

    def _change_pauses(self, chat_id):
        """Change anything related to a pause if it is over the time limit."""
        for pause_type in self.pause_info[chat_id]:
            if (self.pause_info[chat_id][pause_type] is not None and
                    time.time() > self.pause_info[chat_id][pause_type]):
                self.info.toggle(chat_id, pause_type)
                self.pause_info[chat_id][pause_type] = None

    def _send_something(self, func, arg, silent=False):
        """Apply the given function to all enabled chats."""
        for chat_id in self.info:
            self._change_pauses(chat_id)
            if self.info[chat_id]['enabled']:
                if self.info[chat_id]['silent']:
                    silent = True
                func(chat_id, arg, disable_notification=silent)

    def send_message(self, message, silent=False):
        """Send a given message to all enabled chats."""
        self._send_something(self.updater.bot.send_message, message, silent)

    def _send_photo(self, chat_id, frame, *args, **kwargs):
        with TempImage(frame) as fp:
            self.updater.bot.send_photo(chat_id, fp, *args, **kwargs)

    def send_photo(self, frame, silent=False):
        """Send a given photo to all enabled chats."""
        self._send_something(self._send_photo, frame, silent)

    def _update_pauses(self, update, change_type):
        message_parts = update.message.text.split()
        if len(message_parts) == 1:
            self.pause_info[str(update.effective_chat.id)][change_type] = None
        else:
            self.pause_info[str(update.effective_chat.id)][change_type] = \
                time.time() + int(message_parts[1])*60

    def _reply_and_log(self, name, update, log=True):
        chat_id = update.effective_chat.id
        if name == 'enabled':
            if self.info[chat_id]['enabled']:
                text = 'Camera Enabled'
            else:
                text = 'Camera Disabled'
        elif name == 'silent':
            if self.info[chat_id]['silent']:
                text = 'Notifications Disabled'
            else:
                text = 'Notifications Enabled'
        if self.pause_info[str(chat_id)][name] is not None:
            seconds = int(self.pause_info[str(chat_id)][name] - time.time())
            minutes = seconds // 60
            seconds %= 60
            hours = minutes // 60
            minutes %= 60
            if hours > 0:
                text += f' for {hours}h{minutes}m{seconds}s'
            elif minutes > 0:
                text += f' for {minutes}m{seconds}s'
            else:
                text += f' for {seconds}s'
        update.message.reply_text(text)
        if log:
            self.logger.info(f'{text} for {chat_id}')

    def _start(self, update, context):
        self.logger.info(f'{update.message.text}')
        if update.message.text == '/start lego':
            self.info.create_new(update.effective_chat.id)
            self.pause_info[str(update.effective_chat.id)] = \
                {'enabled': None, 'silent': None}
            update.message.reply_text('Password correct')
            self.logger.info(f'Added Chat {update.effective_chat.id}')

    @password
    def _disable(self, update, context):
        self._update_pauses(update, 'enabled')
        self.info.setitem(update.effective_chat.id, 'enabled', False)
        self._reply_and_log('enabled', update)

    @password
    def _enable(self, update, context):
        self._update_pauses(update, 'enabled')
        self.info.setitem(update.effective_chat.id, 'enabled', True)
        self._reply_and_log('enabled', update)

    @password
    def _status(self, update, context):
        self._change_pauses(str(update.effective_chat.id))
        self._reply_and_log('enabled', update, log=False)
        self._reply_and_log('silent', update, log=False)

    @password
    def _picture(self, update, context):
        global frame  # Yes I know this is a bit dodgy
        with TempImage(frame) as fp:
            update.message.reply_photo(fp)

    @password
    def _silent(self, update, context):
        self._update_pauses(update, 'silent')
        self.info.toggle(update.effective_chat.id, 'silent')
        self._reply_and_log('silent', update)


def check_threshold(frame_delta):
    """Check if a frame_delta crosses the change threshold."""
    thresh = cv2.threshold(frame_delta, 5, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh, None, iterations=2)
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    bounding_boxes = []
    for c in cnts:
        if cv2.contourArea(c) > 5000:
            bounding_boxes.append(cv2.boundingRect(c))
    return bounding_boxes


if __name__ == '__main__':
    try:
        if not path.isdir(f'{PATH}/logs'):
            mkdir(f'{PATH}/logs')
        logging.basicConfig(
            filename=f'{PATH}/logs/{datetime.now():%Y%m%dT%H%M%S}.log',
            filemode='w',
            format='%(levelname)s %(asctime)s - %(message)s',
            level=logging.INFO
        )
        logger = logging.getLogger()

        time.sleep(10)

        bot = TelegramBot(logger)
        bot.send_message('Starting up...', silent=True)

        camera = PiCamera()
        camera.resolution = (640, 480)
        camera.framerate = 16
        raw_capture = PiRGBArray(camera, size=(640, 480))
        logger.info('Warming up...')
        time.sleep(2.5)
        avg = None
        last_sent = -1
        motion = False

        for f in camera.capture_continuous(raw_capture, format='bgr',
                                           use_video_port=True):
            frame = f.array
            timestamp = datetime.now()
            frame = imutils.resize(frame, width=500)
            grey = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            grey = cv2.GaussianBlur(grey, (21, 21), 0)
            if avg is None:
                logger.info('Starting background model...')
                avg = grey.copy().astype('float')
                raw_capture.truncate(0)
                continue
            cv2.accumulateWeighted(grey, avg, 0.5)
            frame_delta = cv2.absdiff(grey, cv2.convertScaleAbs(avg))
            boxes = check_threshold(frame_delta)
            if boxes != []:
                for box in boxes:
                    cv2.rectangle(frame, (box[0], box[1]),
                                  (box[0]+box[2], box[1]+box[3]),
                                  (0, 255, 0), 2)
                if not motion:
                    motion = True
                    logger.info('Motion Detected!')
                if time.time() - last_sent > 2:
                    last_sent = time.time()
                    bot.send_photo(frame)
            else:
                if motion:
                    motion = False
                    logger.info('Motion Stopped')
            raw_capture.truncate(0)
    except:  # noqa
        logger.exception('')
        bot.send_message('Crashed', silent=True)
        raise
